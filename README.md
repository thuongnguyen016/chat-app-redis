### Yêu cầu

1. [Redis](https://github.com/MicrosoftArchive/redis/releases)
2. Laravel Echo
3. [Laravel Echo Server]
4. [Socket.io]

### Cài đặt

#### Download

Clone source từ gitlab, cài đặt thư viện.

```bash
git clone abc
cd abc
```

#### Environment Files

- Copy file .env từ env-sample hoặc env-example

```bash
cp .env-example .env
```

#### Create Database

- Cập nhật thông tin database tương ứng

```bash
DB_DATABASE=DB_NAME
DB_USERNAME=root
DB_PASSWORD=
```

#### Artisan Commands

- Tạo key để Laravel sử dụng mã hóa.

```bash
php artisan key:generate
```

- Tạo table cho database và seed dữ liệu mặc định.

```bash
php artisan migrate:fresh --seed | composer mfs
```
### Hướng dẫn để tạo source chat với redis
- https://viblo.asia/p/viet-ung-dung-chat-realtime-voi-laravel-vuejs-redis-va-socketio-laravel-echo-Qpmle9Q9lrd#_iv-real-time-3

#### Git Hooks

```bash
php artisan git:install-hooks
```

#### Storage:link

- Sau khi cài đặt thì chạy lệnh này để link thư mục public với storage.

```bash
php artisan storage:link
```

#### Code
     
- Chạy local server của php

```bash
php artisan serve
```

- Lần đầu tiên clone source về thì chạy lệnh:
```bash
yarn run build-all
```

#### Asset
- Để build js và css:

```bash
yarn run dev | yarn run watch
```

- Để build core js:

```bash
yarn run development --core-js
```

- Để build core sass:

```bash
yarn run development --core-sass
```

- Để build core js và core sass:

```bash
yarn run development --core
```

- Để build core theme:

```bash
yarn run development --theme
```

- Để build tất cả asset:

```bash
yarn run development --all
```

## Thư viện

### Backend
1. [Ziggy](https://github.com/tightenco/ziggy)
1. [Laravel Permission](https://github.com/spatie/laravel-permission)
1. [Laravel ActivityLog](https://github.com/spatie/laravel-activitylog)
1. [Laravel Excel](https://github.com/Maatwebsite/Laravel-Excel) và [PhpSpreadsheet](https://github.com/PHPOffice/PhpSpreadsheet)

### Frontend
1. [axios](https://github.com/axios/axios)
1. [FormValidation](https://formvalidation.io/)
1. [datatables](https://datatables.net/)
1. [sweetalert2](https://github.com/sweetalert2/sweetalert2)
1. [lodash](https://lodash.com/docs/)
1. [numeral](http://numeraljs.com/)
1. [highcharts](https://github.com/highcharts/highcharts)
1. [jquery](https://jquery.com/)
1. [jquery.alphanum](https://github.com/KevinSheedy/jquery.alphanum)
1. [cleave](https://github.com/nosir/cleave.js)
