<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Resources\Order as OrderResource;
use App\Models\Order;
use Illuminate\Http\Request;

class OrdersController extends ApiController
{
	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
	 * @SuppressWarnings(PHPMD)
	 */
	public function createOrder(Request $request)
	{
		$request->validate([
			'code' => 'required',
		]);

		try {
//		\DB::beginTransaction();

			$requestData                = $request->all();
			$customer_name              = $requestData['customer_name'] ?? '';
			$customer_phone             = $requestData['customer_phone'] ?? '';
			$customer_address           = $requestData['customer_address'] ?? '';
			$customer_email             = $requestData['customer_email'] ?? '';
			$customer                   = \App\Models\Customer::where('phone', $customer_phone)->firstOrCreate([
				'name'       => $customer_name,
				'phone'      => $customer_phone,
				'address'    => $customer_address,
				'email'      => $customer_email,
				'city_id'    => 59,
				'county_id'  => 675,
				'ward_id'    => 10774,
				'created_by' => 1,
			]);
			$requestData['customer_id'] = $customer->id;
			$order                      = \App\Models\Order::create($requestData);
			\App\Models\OrderDetail::insertOrderDetail($requestData, $order);

//		\DB::commit();

			$redirectUrl = route('orders.index');

			return response()->json([
				'message'      => __('Data created successfully'),
				'redirect_url' => $redirectUrl,
			]);
		} catch (\Exception $e) {
			\DB::rollBack();

			return response([$e], 400);
		}
	}

	public function orders()
	{
		return [
			'result'  => true,
			'message' => 'OK',
			'data'    => OrderResource::collection(Order::all()),
		];
	}

	public function changeOrderState(Order $order, Request $request)
	{
		$order->update($request->all());
		return [
			'result'	=> true,
			'message'	=> 'OK',
			'data'		=> $order,
		];
	}
}