<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\Category;
use Illuminate\Http\JsonResponse;

/**
 * Class CategoriesController
 * @package App\Http\Controllers\Api
 */
class CategoriesController extends ApiController
{
	/**
	 * Lấy danh sách danh mục
	 *
	 * @return JsonResponse
	 */
	public function categories()
	{
		$rootCategories = Category::with('subCategories')->active()->root()->get();

		$categoryCollection = \App\Http\Resources\Category::collection($rootCategories);

		return $this->asJson([
			'message' => 'OK',
			'data'    => $categoryCollection,
			'result'  => true,
		]);
	}
}