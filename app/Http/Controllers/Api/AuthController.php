<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\User;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends ApiController
{
	public function signUp(Request $request)
	{
		$request->validate([
			'username' => 'required|string',
			'password' => 'required|string|confirmed',
		]);
		$user = new User([
			'username' => $request->username,
			'password' => Hash::make($request->password),
		]);
		$user->save();

		return $this->asJson([
			'message' => 'Successfully created user!',
		], 201);
	}

	public function signIn(Request $request)
	{
		$request->validate([
			'username' => 'required|string',
			'password' => 'required|string',
//            'remember_me' => 'boolean',
		]);

		$username = $request->get('username');
		$password = $request->get('password');

		info("$username - $password");

		if (Auth::attempt(['username' => $username, 'password' => $password])) {
			$user        = $request->user();
			$tokenResult = $user->createToken('Personal Access Token');
			$token       = $tokenResult->token;
			if ($request->remember_me) {
				$token->expires_at = Carbon::now()->addWeeks(1);
			}
			$token->save();

			return $this->asJson([
				'result_code'  => true,
				'message'      => __('Success'),
				'access_token' => $tokenResult->accessToken,
				'token_type'   => 'Bearer',
				'expires_at'   => Carbon::parse(
					$tokenResult->token->expires_at
				)->toDateTimeString(),
			]);
		}

		return $this->asJson([
			'result_code' => false,
			'message'     => 'Đăng nhập thất bại. Tên đăng nhập hoặc mật khẩu không chính xác.',
		], 401);
	}

	public function signOut(Request $request)
	{
		$request->user()->token()->revoke();

		return $this->asJson([
			'message' => 'Successfully logged out',
		]);
	}

	/**
	 *
	 * @param Request $request
	 *
	 * @return JsonResponse
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function changePassword(Request $request)
	{
		$this->validate($request, [
				'password' => [
					'required',
					'confirmed',
					'min:8',
					'regex:/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/',
				],
			]
		);
		$currentPassword = request()->get('current_password');
		$password        = request()->get('password');

		if ( ! $currentPassword || ! $password) {
			return $this->asJson([
				'message' => 'Tham số không hợp lệ.',
			], 400);
		}

		/** @var User $user */
		$user = request()->user();

		if ($user && $user->changePassword($currentPassword, $password)) {
			return $this->asJson([
				'message' => 'Thay đổi mật khẩu thành công.',
			]);
		}

		return $this->asJson([
			'message' => 'Thay đổi mật khẩu thất bại.',
		], 402);
	}

	/**
	 * Get the authenticated User
	 *
	 * @return JsonResponse [json] user object
	 */
	public function getUser()
	{
		$user = request()->user();

		if ( ! $user) {
			return $this->asJson([
				'result_code' => false,
				'message'     => 'Không tìm thấy thông tin user',
				'data'        => '',
			], 400);
		}

		$file = storage_path('app/public/users/' . $user->avatar);

		$data = [
			'name'       => $user->name,
			'department' => $user->department->name,
			'position'   => $user->position->name,
			'email'      => $user->email,
			'username'   => $user->username,
			'avatar'     => $user->avatar && file_exists($file) ?
				base64_encode(file_get_contents($file)) :
				'',
		];

		return $this->asJson([
			'result_code' => true,
			'message'     => 'Thành công',
			'data'        => $data,
		]);
	}
}