<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Resources\Product as ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends ApiController
{
	private function responseAsFormat($status, $message, $data)
	{
		return ['result' => $status, 'message' => $message, 'data' => $data];
	}

	public function products(Request $request)
	{
		try {
			$query = Product::query()->with(['productImages', 'productVariants']);

			$limit   = $request->limit ?? 10;
			$offset  = $request->offset ?? 0;
			$perPage = $request->perPage ?? 5;
			$keySort = $request->keySort ?? 'asc';

			if ( ! empty($keySort)) {
				$query->orderBy('id', strtolower($keySort));
			}

			$query->when($request->brand_id, function ($brandId) use ($request) {
				$brandId->where('brand_id', $request->brand_id);
			})->when($request->category_id, function ($categoryId) use ($request) {
				$categoryId->whereHas('categories', function ($q) use ($request) {
					$q->where('category_id', $request->category_id);
				});
			});

			$products = $query->take($limit)
			                  ->skip($offset)
			                  ->paginate($perPage);

			$response = $this->responseAsFormat(true, __('Success'), \App\Http\Resources\ProductList::collection($products));

			return $this->asJson($response);
		} catch (\Exception $exception) {
			$response            = [];
			$response['result']  = false;
			$response['message'] = __('No search result found');
			if (config('app.debug')) {
				$response['message'] = $exception->getMessage();
			}

			return $this->asJson($response, 404);
		}
	}

	/**
	 * @param Product $product
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @SuppressWarnings(PHPMD)
	 */
	public function getProductDetail(Product $product)
	{
		try {
			$response = ProductResource::make($product);

			if ($response) {
				$response = $this->responseAsFormat(true, __('Success'), $response);
			}

			return $this->asJson($response);
		} catch (\Exception $e) {
			$response            = [];
			$response['result']  = false;
			$response['message'] = __('No search result found');
			if (config('app.debug')) {
				$response['message'] = $e->getMessage();
			}

			return $this->asJson($response, 404);
		}
	}
}