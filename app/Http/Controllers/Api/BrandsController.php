<?php

namespace App\Http\Controllers\Api;

use App\Enums\State\BrandState;
use App\Http\Controllers\ApiController;
use App\Models\Brand;
use Illuminate\Http\JsonResponse;

/**
 * Class BrandsController
 * @package App\Http\Controllers\Api
 */
class BrandsController extends ApiController
{
	/**
	 * Lấy danh sách thương hiệu
	 *
	 * @return JsonResponse
	 */
	public function brands()
	{
		$brands = Brand::active()->get();

		return $this->asJson([
			'message' => 'OK',
			'data'    => \App\Http\Resources\Brand::collection($brands),
			'result'  => true,
		]);
	}
}