<?php

namespace App\Models;

use App\Models\Traits\Relationships\PostRelationship;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory, PostRelationship;

    protected $fillable = [
        'user_id',
        'title',
        'description',
    ];
}
