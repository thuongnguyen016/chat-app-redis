<?php
/**
 * User: test
 * Date: 10/12/2020 2:51 PM
 */

namespace App\Models\Traits\Relationships;


use App\Models\User;

trait PostRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
