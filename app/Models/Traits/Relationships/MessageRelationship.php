<?php
/**
 * User: test
 * Date: 10/13/2020 10:46 AM
 */

namespace App\Models\Traits\Relationships;

use App\Models\User;

trait MessageRelationship
{

    public function user() {
        return $this->belongsTo(User::class);
    }
}
