<?php
/**
 * User: test
 * Date: 10/13/2020 10:47 AM
 */

namespace App\Models\Traits\Relationships;

use App\Models\Message;

trait UserRelationship
{
    public function messages() {
        return $this->hasMany(Message::class);
    }
}
