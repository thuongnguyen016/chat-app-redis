<?php

namespace App\Models\Traits\Commons;

use App\Enums\ActionLog;

trait Logable
{
	/**
	 * @param $historyClass
	 * @param $description
	 * @param $action
	 * @param $state
	 * @param $targetField
	 *
	 * @return mixed
	 */
	private function saveLog($historyClass, $description, $action, $state, $targetField)
	{
		return $historyClass::create([
			'description' => $description,
			'action'      => $action,
			'state'       => $state,
			$targetField  => $this->id,
			'created_by'  => auth()->id() ?? 1,
			'properties'  => $this->attributesToArray(),
			'created_at'  => now()->toDateTimeString(),
		]);
	}

	public function logCreate($historyClass, $description, $state, $targetField)
	{
		return $this->saveLog($historyClass, $description, ActionLog::CREATE, $state, $targetField);
	}

	public function logUpdate($historyClass, $description, $state, $targetField)
	{
		return $this->saveLog($historyClass, $description, ActionLog::UPDATE, $state, $targetField);
	}

	public function logDelete($historyClass, $description, $state, $targetField)
	{
		return $this->saveLog($historyClass, $description, ActionLog::DELETE, $state, $targetField);
	}
}
