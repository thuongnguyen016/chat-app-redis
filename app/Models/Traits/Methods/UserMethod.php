<?php
/**
 * User: test
 * Date: 10/12/2020 4:36 PM
 */
namespace App\Models\Traits\Methods;
trait UserMethod
{
    public function follow($userId)
    {
        $this->follows()->attach($userId);

        return $this;
    }

    public function unfollow($userId)
    {
        $this->follows()->detach($userId);

        return $this;
    }

    public function isFollowing($userId)
    {
        return (boolean) $this->follows()->where('follows_id', $userId)->first(['id']);
    }
}
