<?php

namespace App\Models\Traits\Scopes;

use App\Enums\Business\CategoryType;
use App\Enums\State\CategoryState;
use Illuminate\Database\Eloquent\Builder;

/**
 * @link https://laravel.com/docs/master/eloquent#query-scopes
 */
trait CategoryScope
{
	public function scopeActive(Builder $query): Builder
	{
		return $query->where(['state' => CategoryState::SHOW]);
	}

	public function scopeRoot(Builder $query): Builder
	{
		return $query->where(['type' => CategoryType::ROOT]);
	}

	public function scopeSub(Builder $query): Builder
	{
		return $query->where(['type' => CategoryType::SUB]);
	}
}
