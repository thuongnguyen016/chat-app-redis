<?php

namespace App\Models\Traits\Scopes;

use App\Enums\State\BrandState;
use Illuminate\Database\Eloquent\Builder;

/**
 * @link https://laravel.com/docs/master/eloquent#query-scopes
 */
trait BrandScope
{
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('state', BrandState::ACTIVE);
    }
}
