<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        User::create([
            'name'              => $faker->name,
            'password'          => \Hash::make(123456),
            'email'             => 'admin@gmail.vn',
            'email_verified_at' => now(),
            'remember_token'    => Str::random(10),
        ]);

        User::factory()->count(5)->create();
//        factory(User::class, 5)->create();
    }
}
